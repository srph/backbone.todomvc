var app = app || {};

// The Application
// ----------

app.AppView = Backbone.View.extend({
	// Instead of generating a new element, bind to the existing skeleton of
	// the App already present in the HTML.
	el: '#todoapp',

	// Set the template for the stats (at the bottom)
	statsTemplate: _.template($('#stats-template').html()),

	events: {
		'keypress #new-todo': 'createOnEnter',
		'click #clear-completed': 'clearCompleted',
		'click #toggle-all': 'toggleAllCompleted'
	},

	// At initialization, we bind to the relevant events on the 'Todos'
	// collection when items are added or changed. Begins by loading
	// any pre-existing todos that might be saved in the 'localStorage'
	initialize: function() {
		this.allCheckbox = this.$('#toggle-all')[0];
		this.$input = this.$('#new-todo');
		this.$footer = this.$('#footer');
		this.$main = this.$('#main');

		this.listenTo(app.Todos, 'add', this.addOne);
		this.listenTo(app.Todos, 'reset', this.addAll);

		this.listenTo(app.Todos, 'change: completed', this.filterOne);
		this.listenTo(app.Todos, 'filter', this.filterAll);
		this.listenTo(app.Todos, 'all', this.render);

		// Fetch
		app.Todos.fetch();
	},

	// Re-rendering the app: refreshing the statistics portion
	render: function() {
		var completed = app.Todos.completed().length;
		var remaining = app.Todos.remaining().length;

		if(app.Todos.length) {
			this.$main.fadeIn();
			this.$footer.fadeIn();

			this.$footer.html(this.statsTemplate({
				completed: completed,
				remaining: remaining
			}));

			this.$('#filters li a')
				.removeClass('selected')
				.filter('[href="#/' + (app.TodoFilter || '' ) + '"]')
				.addClass('selected');
		} else {
			this.$main.fadeOut();
			this.$footer.fadeOut();
		}

		this.allCheckbox.checked = !remaining;
	},

	// Add a single todo item to the list by creating a view,
	// and appending its element to the ul.
	addOne: function(todo) {
		var view = new app.TodoView({ model: todo });
		this.$input.val('');
		$('#todo-list').append(view.render().el);
	},

	addAll: function() {
		this.$('#todo-list').html('');
		app.Todos.each(this.addOne, this);
	},

	filterOne: function(todo) {
		todo.trigger('visible');
	},

	filterAll: function() {
		app.Todos.each(this.filterOne, this);
	},

	// Generate the attributes for the new Todo item
	newAttributes: function() {
		return {
			title: this.$input.val().trim(),
			order: app.Todos.nextOrder(),
			completed: false
		};
	},

	// If the return button was hit in the main input field, create a new Todo
	// model, storing it to the local storage
	createOnEnter: function() {
		if( event.which !== ENTER_KEY || !this.$input.val().trim()) {
			return;
		}

		// Create the todo item
		app.Todos.create(this.newAttributes());
	},

	// Clear all completed, destroying its model
	clearCompleted: function() {
		_.invoke(app.Todos.completed(), 'destroy');
		return false;
	},

	toggleAllCompleted: function() {
		var completed = this.allCheckbox.checked;

		app.Todos.each(function(todo) {
			todo.save({
				'completed': completed
			});
		});
	}
});