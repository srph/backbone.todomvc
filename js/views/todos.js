var app = app || {};

// Todo Item View
// ----------

// DOM element for an item
app.TodoView = Backbone.View.extend({

	// a list tag
	tagName: 'li',

	// Cache the template function for a single item
	template: _.template($('#item-template').html()),

	// DOM events specified to an item
	events: {
		'click .toggle': 'toggleCompleted',
		'dblclick label': 'edit',
		'click .destroy': 'clear',
		'keypress  .edit': 'updateOnEnter',
		'blur .edit': 'close'
	},

	// The Todo View listens for changes to its model, re-rendering. Since
	// there is a one-to-one correspondence between a **Todo** and a **TodoView**
	// in this app, we set a direct reference on the model for convenience.
	initialize: function() {
		this.listenTo(this.model, 'change', this.render);
		this.listenTo(this.model, 'destroy', this.remove);
		this.listenTo(this.model, 'visible', this.toggleVisible);
	},

	// Re-render the item title
	render: function() {
		this.$el.html( this.template( this.model.toJSON() ) );

		this.$el.toggleClass('completed', this.model.get('completed'));
		this.toggleVisible();

		this.$input = this.$('.edit');
		return this;
	},

	// Toggle the visibility of the item
	toggleVisible: function() {
		this.$el.toggleClass('hidden', this.isHidden());
	},

	// Determines if an item is hidden / should be hidden
	isHidden: function() {
		var isCompleted = this.model.get('completed');
		return (
			(!isCompleted && app.TodoFilter === 'completed')
			|| (isCompleted && app.TodoFilter === 'active')
		);
	},

	// Toggle the completed state
	toggleCompleted: function() {
		this.model.toggle();
	},

	// Switch to editing mode, saving changes to the todo
	edit: function() {
		this.$el.addClass('editing');
		this.$input.focus();
	},

	// Close the editing mode
	close: function() {
		var value = this.$input.val().trim();

		// If value already exists
		if(value) {
			this.model.save({ title: value });
		} else {
			this.clear();
		}

		this.$el.removeClass('editing');
	},

	updateOnEnter: function(e) {
		if(e.which === ENTER_KEY) {
			this.close();
		}
	},

	clear: function() {
		this.model.destroy();
	}
});