var app = app || {};

// Todo Collection
// ----------

var TodoList = Backbone.Collection.extend({
	// Reference to our collection's model
	model: app.Todo,

	// Save all of our todo items under the "todos-backbone" namespaces
	localStorage: new Backbone.LocalStorage('todos-backbone'),

	// Filter down the list of all todo items that are complete
	completed: function() {
		return this.filter(function(todo) {
			return todo.get('completed');
		});
	},

	// Filter down the list of all todo items that have not yet
	// been marked as complete
	remaining: function() {
		return this.without.apply(this, this.completed())
	},

	// Keep the list in sequential order
	nextOrder: function() {
		// If it is blank, set its order to 1 (first)
		if(!this.length) {
			return 1;
		}

		// Otherwise, set its order to number next to the last
		return this.last().get('order') + 1;
	},

	// Todos are sorted by their original insertion order
	comparator: function(todo) {
		return todo.get('order');
	}
});	

// Create global collection of Todos
app.Todos = new TodoList();