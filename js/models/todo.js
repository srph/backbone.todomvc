var app = app || {};

// Todo Model
// ----------
// Basic *TODO* Model
// Attributes: title, order, completed

app.Todo = Backbone.Model.extend({
	// All defaults attributes of a new
	// instances of this model
	defaults: {
		title: '',
		completed: false
	},

	// Toggle the 'completed' state of this todo item
	toggle: function() {
		this.save({
			completed: !this.get('completed')
		})
	}
});