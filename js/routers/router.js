
// Todo Routes
// ----------
// Basic *TODO* Model
// Attributes: title, order, completed
var Workspace = Backbone.Router.extend({
	routes: {
		'*filter': 'setFilter'
	},

	setFilter: function(param) {
		// Set current filter to be used
		if(param) {
			param = param.trim();
		}

		app.TodoFilter = param || '';

		// Trigger all collection filter event
		app.Todos.trigger('filter');
	}
});

app.TodoRouter = new Workspace();
Backbone.history.start();